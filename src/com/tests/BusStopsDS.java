package com.tests;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class BusStopsDS {
    private final Sql dbHelper;
    private final String[] allColumnsBusStops = {Sql._ID,
            Sql.bus_stop_CODE, Sql.bus_stop_NAME};
    private final String[] allColumnsBusLine = {Sql._ID, Sql.bus_line_CODE,
            Sql.bus_line_DIRECTION, Sql.bus_line_LINE, Sql.bus_line_NAME};
    private SQLiteDatabase db;

    public BusStopsDS(Context context) {
        dbHelper = new Sql(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public BusStops createBusStop(String code, String name) {
        ContentValues values = new ContentValues();
        values.put(Sql.bus_stop_CODE, code.substring(0, Math.min(5, code.length())));
        values.put(Sql.bus_stop_NAME, name);

        long insertID = db.insert(Sql.TABLE_BUS_STOPS, null, values);
        Cursor cursor = db.query(Sql.TABLE_BUS_STOPS, allColumnsBusStops, Sql._ID
                + " = " + insertID, null, null, null, null);

        cursor.moveToFirst();
        BusStops new_bus_stop = cursorToBusStop(cursor);
        cursor.close();
        return new_bus_stop;
    }

    public List<BusStops> getAllBusStops() {
        List<BusStops> lsBS = new ArrayList<BusStops>();

        Cursor cursor = db.query(Sql.TABLE_BUS_STOPS, allColumnsBusStops, null, null,
                null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            BusStops bs = cursorToBusStop(cursor);
            lsBS.add(bs);
            cursor.moveToNext();
        }
        cursor.close();
        return lsBS;

    }

    private BusStops cursorToBusStop(Cursor cursor) {
        BusStops bs = new BusStops();
        bs.setId(cursor.getLong(0));
        bs.setCode(cursor.getString(1));
        bs.setName(cursor.getString(2));
        return bs;

    }

    public void addRelationLineStop(String BusLineCode, String BusStopCode) {
        ContentValues values = new ContentValues();
        values.put(Sql.bus_line_CODE, BusLineCode);
        values.put(Sql.bus_stop_CODE, BusStopCode);
        db.insert(Sql.TABLE_BUS_RELATION, null, values);
    }

    public BusLines createBusLine(String BusLineCode, String direction, Integer line, String name) {

        ContentValues values = new ContentValues();
        values.put(Sql.bus_line_CODE, BusLineCode.substring(0, Math.min(100, BusLineCode.length())));
        values.put(Sql.bus_line_DIRECTION, direction);
        values.put(Sql.bus_line_LINE, line);
        values.put(Sql.bus_line_NAME, name);

        long insertID = db.insert(Sql.TABLE_BUS_LINES, null, values);
        Cursor cursor = db.query(Sql.TABLE_BUS_LINES, allColumnsBusLine, Sql._ID
                + " = " + insertID, null, null, null, null);

        cursor.moveToFirst();
        BusLines add_bus_line = cursorToBusLine(cursor);
        cursor.close();

        return add_bus_line;

    }

    public List<BusLines> getAllBusLines() {
        List<BusLines> lsBL = new ArrayList<BusLines>();

        Cursor cursor = db.query(Sql.TABLE_BUS_LINES, allColumnsBusLine, null, null,
                null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            BusLines bl = cursorToBusLine(cursor);
            lsBL.add(bl);
            cursor.moveToNext();
        }
        cursor.close();
        return lsBL;

    }

    private BusLines cursorToBusLine(Cursor cursor) {
        BusLines bl = new BusLines();
        bl.setId(cursor.getInt(0));
        bl.setBusCode(cursor.getString(1));
        bl.setDirection(cursor.getString(2));
        bl.setLine(cursor.getInt(3));
        bl.setName(cursor.getString(4));
        return bl;

    }

    public void EmptyDatabase() {
        db.delete(Sql.TABLE_BUS_STOPS, null, null);
       // db.delete("sqlite_sequence", "name=" + Sql.TABLE_BUS_STOPS, null);
        db.delete(Sql.TABLE_BUS_LINES, null, null);
       // db.delete("sqlite_sequence", "name=" + Sql.TABLE_BUS_LINES, null);
        db.delete(Sql.TABLE_BUS_RELATION, null, null);
        //db.delete("sqlite_sequence", "name=" + Sql.TABLE_BUS_RELATION, null);
    }
}
