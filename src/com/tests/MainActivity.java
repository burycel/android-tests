package com.tests;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {

    //    private final webDl webdl = new webDl();
    private ListView lv;
    private BusStopsDS bsDS;
    private List<BusLines> valuesBusLines;
    private ArrayAdapter<BusLines> adapterBusLines;
    private List<BusStops> valuesBusStops;
    private ArrayAdapter<BusStops> adapterBusStops;
    private List<String> valuesTimes;
    private ArrayAdapter<String> adapterTimes;
    private Button btnBusLines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnBusLines = (Button) findViewById(R.id.ShowBusLines);
        bsDS = new BusStopsDS(this);
        bsDS.open();


        lv = (ListView) findViewById(R.id.listView1);
        EditText et1 = (EditText) findViewById(R.id.editText1);
        et1.setImeActionLabel("Done", KeyEvent.KEYCODE_ENTER);
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                if (lv.getAdapter() == adapterBusLines && (adapterBusLines != null)) {
                    adapterBusLines.getFilter().filter(charSequence);
                } else if (lv.getAdapter() == adapterBusStops && (adapterBusStops != null)) {
                    adapterBusStops.getFilter().filter(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BusLines bl = (BusLines) adapterView.getItemAtPosition(i);
                Toast.makeText(getApplicationContext(),
                        "Clicked on Row: " + bl.getName(),
                        Toast.LENGTH_SHORT).show();
                //  Toast.makeText(this, "Test" + i ,1000).show();
            }
        });

//		 values = blDS.getAllBusLines();
//		 adapter = new ArrayAdapter<BusLines>(this,
//		 android.R.layout.simple_list_item_1,values);

//		 lv.setAdapter(adapter);
//		 setListAdapter(adapter);
//


        // new DownloadFilesTask(this).execute("mata");
    }

    public void onClick(View view) {
        // @SuppressWarnings("unchecked")
//		adapter = new ArrayAdapter<BusLines>(this,
//				 android.R.layout.simple_list_item_1, values);
//		BusLines bl = null;
        switch (view.getId()) {
            case R.id.add:
                new DownloadFilesTask(this).execute("mata");
                break;
            case R.id.ShowBusLines:
                valuesBusLines = bsDS.getAllBusLines();
                adapterBusLines = new ArrayAdapter<BusLines>(this,
                        android.R.layout.simple_list_item_1, valuesBusLines);
                Toast.makeText(this, adapterBusLines.getCount() + " rows", 1000).show();


                lv.setAdapter(adapterBusLines);
/*
if (lv.getAdapter().getCount() > 0) {
bl = (BusLines) values.get(0);
blDS.DeleteBusLine(bl);
adapter.remove(bl);
}
*/
                break;
            case R.id.ShowBusStops:
                valuesBusStops = bsDS.getAllBusStops();
                adapterBusStops = new ArrayAdapter<BusStops>(this,
                        android.R.layout.simple_list_item_1, valuesBusStops);
                Toast.makeText(this, adapterBusStops.getCount() + " rows", 1000).show();

                lv.setAdapter(adapterBusStops);
        }
//		adapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private class DownloadFilesTask extends AsyncTask<String, Integer, String> {
        final Context context;
        ProgressDialog dialog;


        public DownloadFilesTask(Context context) {
            this.context = context;
        }

        protected void onPreExecute() {
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setTitle("Downloading ...");
            dialog.setMax(128);
            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {
            bsDS = new BusStopsDS(this.context);
            bsDS.open();
            Log.e("BuryTest", "Passed");
//            bsDS.EmptyDatabase();

            try {
                /*
                table tr:eq(1) td:eq(2) div
                time Integer not null (12359,20450,32359-> 10000)(20:58 -> 2058+10000 -> 12058)
                )
                */
//                Document doc = Jsoup.connect("http://195.193.209.12/gvbpublicatieinternet/SelectHalteLijn.aspx").get();
//                Elements elemBusLines = doc.select("#allelijnen option");
//                for (Element elemBusLine: elemBusLines) {
//
//                   String tempValue[] = elemBusLine.attr("value").split("\\|");
//
//                   String bus_code = tempValue[2];
//                   String direction = tempValue[1];
//                   Integer line = Integer.parseInt(tempValue[0]);
//                    String name = elemBusLine.text().split(":\\s")[1];
//                    BusLines blInserted = bsDS.createBusLine(bus_code,direction , line, name);
//
//                    // http://195.193.209.12/gvbpublicatieinternet/SelectHalteLijn.aspx?lijnnummer=1&richting=H&key=1ab57a4b-ed2d-4100-9b7f-456ff4e8fccb
//
//                    String build_url;
//                    build_url = "http://195.193.209.12/gvbpublicatieinternet/SelectHalteLijn.aspx?lijnnummer=";
//                    build_url += blInserted.getLine().toString();
//                    build_url += "&richting=";
//                    build_url += blInserted.getDirection();
//                    build_url += "&key=";
//                    build_url += blInserted.getBusCode();
//
//                    Document docRelation = Jsoup.connect(build_url.toString()).get();
//                    Elements elemRelations = docRelation.select("#allehaltes option");
//                    for (Element elemRelation: elemRelations) {
//                        bsDS.createBusStop(elemRelation.attr("value"), elemRelation.text());
//                        bsDS.addRelationLineStop(blInserted.getId().toString(),elemRelation.attr("value"));
//                    }
//                 publishProgress(1);
//                }
// http://195.193.209.12/gvbpublicatieinternet/TimeTable.aspx?type=hvs
// &lijnnummer=35&haltecode=01211&richting=T&key=0b2b2656-754a-4cc1-a57d-ad8451804ff1
                String build_url = "http://195.193.209.12/gvbpublicatieinternet/TimeTable.aspx?type=hvs&lijnnummer=";
                build_url += 35;
                build_url += "&haltecode=";
                build_url += "01210";
                build_url += "&richting=";
                build_url += "H";
                build_url += "&key=";
                build_url += "0b2b2656-754a-4cc1-a57d-ad8451804ff1";
                List<Integer> newTimes = new ArrayList<Integer>();

                Document docTime = Jsoup.connect(build_url.toString()).get();

                Elements elemTables;
                // 1 = 4 , 2 = 5 3 = 6 i+ 3
                for (int i = 1; i < 22; i++) {   //tr:eq(1) is Luni - Vineri
                    elemTables = docTime.select(".uren tr:eq(1) td:eq(" + i + ") div");  // 1 = ora 4
                    for (Element time : elemTables) {
                        String curTime = time.text().replace("\u00a0", "").replace("A", "").trim();
                        if (curTime.length() > 0 && curTime.length() < 3) {
                            if (i < 21) {
                                curTime = String.valueOf(Integer.parseInt((i + 3) + curTime) + 10000);
                            } else if (i == 21) {
                                 curTime = String.valueOf(Integer.parseInt(curTime) + 20000);
                            } else if (i > 21) {
                                curTime = String.valueOf(Integer.parseInt((i - 21) + curTime) + 20000);
                            }
                            newTimes.add(Integer.parseInt(curTime));
                        }
                    }
                }
                for (int i = 1; i < 22; i++) {   //tr:eq(3) is Sambata
                    elemTables = docTime.select(".uren tr:eq(3) td:eq(" + i + ") div");
                    for (Element time : elemTables) {
                        String curTime = time.text().replace("\u00a0", "").replace("A", "").trim();
                        if (curTime.length() > 0 && curTime.length() < 3) {
                            if (i < 21) {    // inseamna azi
                                curTime = String.valueOf(Integer.parseInt((i + 3) + curTime) + 20000);
                            } else if (i == 21) {
                                curTime = String.valueOf(Integer.parseInt(curTime) + 30000);
                            } else if (i > 21) {
                                curTime = String.valueOf(Integer.parseInt((i - 21) + curTime) + 30000);
                            }
                            newTimes.add(Integer.parseInt(curTime));
                        }
                    }
                }
                for (int i = 1; i < 22; i++) {   //tr:eq(5) is Duminica
                    elemTables = docTime.select(".uren tr:eq(5) td:eq(" + i + ") div");
                    for (Element time : elemTables) {
                        String curTime = time.text().replace("\u00a0", "").replace("A", "").trim();
                        if (curTime.length() > 0 && curTime.length() < 3) {
                            if (i < 21) {    // inseamna azi
                                curTime = String.valueOf(Integer.parseInt((i + 3) + curTime) + 30000);
                            } else if (i == 21) {
                                curTime = String.valueOf(Integer.parseInt(curTime) + 10000);
                            } else if (i > 21) {
                                curTime = String.valueOf(Integer.parseInt((i - 21) + curTime) + 10000);
                            }

                            newTimes.add(Integer.parseInt(curTime));
                        }
                    }
                }
                Collections.sort(newTimes);
                for(Integer time: newTimes) {
                    Log.e("complete time", time.toString());
                }

//                elemTables = docTime.select("#allehaltes option");

            } catch (Exception e) {
                Log.e("Error Bury", e.getMessage().toString());
            }
            return null;

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            dialog.incrementProgressBy(1);
        }

        @Override
        protected void onPostExecute(String result) {
            btnBusLines.callOnClick();
            dialog.dismiss();
        }

    }
}
