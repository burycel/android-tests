package com.tests;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Sql extends SQLiteOpenHelper {
	public static final String _ID = "_id";

	public static final String TABLE_BUS_STOPS = "bus_stops";
	public static final String bus_stop_CODE = "bus_stop_code";
	public static final String bus_stop_NAME = "name";

	public static final String TABLE_BUS_LINES = "bus_lines";

	public static final String bus_line_CODE = "bus_line_code"; //text
	public static final String bus_line_DIRECTION = "direction"; //string
    public static final String bus_line_LINE = "line"; // integer
    public static final String bus_line_NAME = "name"; // string

    public static final String TABLE_BUS_RELATION = "bus_line_relation";

    public static final String TABLE_TIME = "time";
    public static final String relation_id = "relation_id";
    public static final String time_of_week = "relation_id";



    private static final String DATABASE_NAME = "GVBury.db";
    private static final int DATABASE_VERSION = 6;

	private String DATABASE_CREATE_TABLE_BUS_STOPS = 
			"create table " + TABLE_BUS_STOPS + "("
			+ _ID + " integer primary key autoincrement," + bus_stop_CODE + " text unique on conflict replace not null , "
			+ bus_stop_NAME + " text not null);";

	private String DATABASE_CREATE_TABLE_BUS_LINES =
			" create table " + TABLE_BUS_LINES + "(" + _ID
			+ " integer primary key autoincrement, " + bus_line_CODE
			+ " text not null, " + bus_line_DIRECTION + " string not null,"
			+ bus_line_LINE + " integer not null, " + bus_line_NAME
			+ " text not null);";
    private String DATABASE_CREATE_LINE_STOP =
            "create table " + TABLE_BUS_RELATION + "("
                    + _ID + " integer primary key autoincrement," + bus_line_CODE + " text not null, "
                    + bus_stop_CODE + " text not null);";
    private String DATABASE_CREATE_TIME =
            "create table " + TABLE_TIME + "("
                    + _ID + " integer primary key autoincrement," + relation_id + " integer not null, "
                    + time_of_week + " integer not null);";

 			
			
	
	
	public Sql(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);	
		//Log.v(DATABASE_CREATE_TABLE_BUS_STOPS, DATABASE_CREATE_TABLE_BUS_LINES);
		
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE_TABLE_BUS_STOPS);
		db.execSQL(DATABASE_CREATE_TABLE_BUS_LINES);
        db.execSQL(DATABASE_CREATE_LINE_STOP);
        db.execSQL(DATABASE_CREATE_TIME);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUS_LINES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUS_STOPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUS_RELATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIME);
		onCreate(db);

	}


	


}
