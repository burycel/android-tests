package com.tests;

public class BusLines {
	private Integer id;
	private String bus_code;
	private String direction;
	private Integer line;
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBusCode() {
		return bus_code;
	}

	public void setBusCode(String bus_code) {
		this.bus_code = bus_code;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
    public Integer getLine() {
        return line;
    }
	public void setLine(Integer line) {
		this.line = line;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
