package com.tests;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class webDl {
    public String getHTMLList(String link, String ListID) throws IOException {
        String result = null;
        HttpClient httpclient = new DefaultHttpClient();

        HttpGet httpget = new HttpGet(link);
        HttpResponse response = httpclient.execute(httpget);

        HttpEntity entity = response.getEntity();


        if (entity != null) {
            InputStream instream = entity.getContent();
            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(instream));
                String aux;
                while ((aux = reader.readLine()) != null) {
                    result += aux;

                }

            } catch (IOException ex) {
                return ex.getMessage();

            } catch (RuntimeException ex) {
                httpget.abort();
                return ex.getMessage();

            } finally {
                instream.close();
            }

            if (result != null) { //make sure it has data
                String tmpDataCompleteList;
                tmpDataCompleteList = result;
                tmpDataCompleteList = tmpDataCompleteList.replaceAll("&#252;", "ü")
                        .replaceAll("&#39;", "'")
                        .replaceAll("&#235;", "ë")
                        .replaceAll("&#246;", "ö");
                tmpDataCompleteList = GetList(tmpDataCompleteList, ListID);

                Pattern replace = Pattern.compile("<option[^>]*?>([^<]+)</option>", Pattern.DOTALL);
                Matcher m = replace.matcher(tmpDataCompleteList);
                while (m.find())
                    if (m.group().length() != 0)

                        result += getValue(m.group(0)) + "@row_splitter@" + getText(m.group(0)) + "@line_splitter@";
            }

        }
        return result;
    }

    private String GetList(String EntireHtml, String IDOfSelect) {
      // Pattern replace = Pattern.compile("<select.*id=\"" + IDOfSelect + "\"[^>]*>(.*?)</select>", Pattern.DOTALL);
//        Pattern replace = Pattern.compile("<select[^.]*id=\""+ IDOfSelect +"\"[^>](.*?)</select>", Pattern.DOTALL);
        Pattern replace = Pattern.compile("<select[^.]{1,50}(id=\"" + IDOfSelect + "\")+[^>](.*?)</select>",
                Pattern.DOTALL);

        Matcher m = replace.matcher(EntireHtml);
        while (m.find()) {
            if (m.group().length() != 0) {
                return m.group(0) + "";
            }
        }
        return null;
    }

    private String getValue(String line) {
        Pattern replace = Pattern.compile("value=\"(.*)\"", Pattern.DOTALL);
        Matcher m = replace.matcher(line.trim());

        while (m.find()) if (m.group().length() != 0) {
            String tmpValue;
            tmpValue = m.group(0).substring(7);
            tmpValue = tmpValue.substring(0, tmpValue.length() - 1);
            return tmpValue;
        }
        return null;

    }

    private String getText(String line) {
        Pattern replace = Pattern.compile(">(.*?)<", Pattern.DOTALL);
        Matcher m = replace.matcher(line.trim());
        while (m.find()) if (m.group().length() != 0) {
            return m.group(0).substring(1).substring(0, m.group(0).length() - 2);
        }
        return null;

    }
}
